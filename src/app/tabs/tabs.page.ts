import { Component } from '@angular/core';
import { profile, ProfileService } from '../profile.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  
  constructor(public ProfileService: ProfileService){
  }

}
