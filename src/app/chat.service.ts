import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from "angularfire2/firestore";
import { Observable } from 'rxjs';
import { map, max } from 'rxjs/operators'
import { Router } from '@angular/router'
import { LoadingController } from '@ionic/angular';
import { Time } from '@angular/common';

export interface message {
  waktu: String;
  user: String;
  isi: String;
}

export interface chat {
  pria: String;
  wanita: String;
  photo_pria: String;
  photo_wanita: String;
  status: String;
  mulai: String;
  last_chat: String;
  pesan: message[];
}

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private chatsCollection: AngularFirestoreCollection<chat>
  public chats: Observable<chat[]>
  public current_chat;
  public current_chat_id;
  public all_chat;
  public id;
  public chat_flag;
  public inactivePage1;
  public inactivePage2;
  public inactivePageCount;

  constructor(db: AngularFirestore, private router: Router, public loadingController: LoadingController) { 
    this.chatsCollection =  db.collection<chat>('chat', ref => ref.orderBy("mulai","desc"));
    this.chats = this.chatsCollection.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data}
        })
      }) 
    )
  }

  setID(id){
    let flag = true;
    this.id = id;
    
    this.all_chat.forEach(chat => {
      try{
        if(chat['id'] == id){
          this.current_chat = chat;
          if(chat['status'] == "aktif"){
            flag = false;
          }
        }
      }catch{
        
      }  
    })   
    return flag;  
  }

  getChat(id){
    return this.chatsCollection.doc<chat>(id).valueChanges();
  }

  updateChat(chat: chat, id: string){
    return this.chatsCollection.doc(id).update(chat)
  }

  addChat(chat: chat){
    this.chatsCollection.add(chat)
  }

  setChat(chat: chat, id: string){
    return this.chatsCollection.doc(id).set(chat);
  }

  removeChat(id){
    return this.chatsCollection.doc(id).delete()
  }
}
