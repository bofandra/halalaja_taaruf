import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from "angularfire2/firestore";
import { Observable } from 'rxjs';
import { map, max, shareReplay, tap, publishReplay, refCount } from 'rxjs/operators'
import { Router } from '@angular/router'
import { Platform, LoadingController } from '@ionic/angular';
import { FCM } from '@ionic-native/fcm/ngx';

export interface profile {
  id: String;
  no_peserta: Number;
  no_pasangan: Number;
  fcm_token: String;
  fcm_token_pasangan: String;
  last_login: String;
  platform: String;
  uid_gmail: String;
  email_google: String;
  photo_google: String;
  gmail_wali: String;
  status: String;
  kelamin: String;
  kulit: String;
  dob: String;
  suku: String;
  domisili: String;
  tinggi: String;
  berat: String;
  status_kawin: String;
  pendidikan: String;
  riwayat_pendidikan: String;
  pekerjaan: String;
  riwayat_pekerjaan: String;
  suka: String;
  tidak_suka: String;
  hafalan: String;
  kajian: String;
  visi: String;
}

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private profilesCollection: AngularFirestoreCollection<profile>
  public profiles: Observable<profile[]>
  public id;
  public current_profile;
  public all_profile = [];
  private profile_notfound = true;
  public user_auth;
  public flag;
  public profile_subscription;
  public current_email;
  public unfilled = [];
  public current_image;
  public user_updated;
  public deleted_user = 0;

  constructor(private fcm: FCM, db: AngularFirestore, private router: Router, public loadingController: LoadingController, private platform: Platform) { 
    var param_date = new Date(Date.now() - new Date(Date.now()).getTimezoneOffset() * 60000);
    param_date.setMonth(param_date.getMonth() - 3);
    //this.profilesCollection =  db.collection<profile>('profile', ref => ref.where("no_peserta",">",0));
    this.user_updated = false;
    this.deleted_user = 0;
    this.profiles = db.collection<profile>('profile', ref => ref.where("no_peserta",">",0)).snapshotChanges().pipe(
      tap(arr=>console.log(arr.length)),
      map(actions =>{
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data}
        })
      }), publishReplay(1), 
      refCount()
    )
  }

  getID(){
    return this.id;
  }

  

  cek_profile(){
    this.unfilled = [];
    let all_field = ["kulit","dob","suku","domisili","tinggi","berat","status_kawin","pendidikan","riwayat_pendidikan","pekerjaan","riwayat_pekerjaan","suka","tidak_suka","hafalan","kajian","visi"]
    let all_field_label = ["Warna kulit","Tanggal lahir","Suku","Domisili","Tinggi badan","Berat badan","Status","Pendidikan terakhir","Riwayat pendidikan","Pekerjaan sekarang","Pengalaman kerja","Hal yang disukai","Hal yang tidak disukai","Hafalan Al-Qur'an","Tempat kajian/ ustadz","Visi-misi berrumah-tangga"]
    
    let i = 0;
    all_field.forEach(field => {
      if(this.current_profile.hasOwnProperty(field)){
        if(this.current_profile[field].length<2){
          this.unfilled.push(all_field_label[i])
        }else{          
        }
      }else{
        this.unfilled.push(all_field_label[i])
      }
      i++;
    });
  }

  check (user){
    if(user['no_peserta'] == -Infinity){
      if(this.deleted_user < 2){
        setTimeout(() => {
          this.removeProfile(user['id']); // run when condition is met
          this.deleted_user++;
        }, 1000);
      }
    }
  }

  async setID(auth2){
    console.log("cek");

    const http = new XMLHttpRequest()
        http.open("GET", "http://halalaja.com/pyud/api/profile/read.php")
        http.send()
        http.onload = () => {
          var profiles = JSON.parse(http.responseText);
          console.log(profiles[0]);
        }
        
    let auth = JSON.parse(JSON.stringify(auth2))
    this.user_auth = auth;
    this.id = auth["uid"];
    this.current_email = auth["email"];
    this.current_image = auth["photoURL"]
    
    const loading = await this.loadingController.create({
      message: 'Menyiapkan aplikasi...',
      translucent: true
    });
    loading.present()

    this.profile_notfound = true;
    this.profile_subscription = this.profiles.subscribe(users=>{
      console.log(users);
      let no_peserta=[];
      this.all_profile = [];
      var i=0;
      this.profile_notfound = true;
      while(i< users.length && this.profile_notfound){
        var user = users[i];
        this.all_profile.push(user);
        this.check(user);
        try{
          if(user['kelamin']=="wanita"){
            if(user['gmail_wali']==this.current_email){
              if(user['no_peserta'] != null && user['no_peserta'] != -Infinity){
                this.current_profile = user;
                this.profile_notfound = false;
              }
            }else{
              if(user['uid_google'] == this.id){
                if(user['no_peserta'] != null && user['no_peserta'] != -Infinity){
                  this.current_profile = user;
                  this.profile_notfound = false;
                }
              }
            }
          }else{
            if(user['uid_google'] == this.id){
              	if(user['no_peserta'] != null && user['no_peserta'] != -Infinity){
                  this.current_profile = user;
                  this.profile_notfound = false;
                }
            }
          }
        }catch{
          
        }  
        
        try{     
          /*if(user['email_google'].includes("bofandra") || user['email_google'].includes("aneeq")){
            console.log(JSON.stringify(user))
          }*/
          if(user['no_peserta']){
            if(user['no_peserta'] != null){
              no_peserta.push(user['no_peserta']);
            }
          }
        }catch{
          
        }
        i++;
      }
      
      if(this.profile_notfound == true){
        //user baru pertama login
        this.current_profile = {
          'uid_google' : this.id,
          'email_google' : this.current_email,
          'photo_google' : this.current_image,
          'no_peserta':Math.max(...no_peserta)+1,
          'platform':JSON.stringify(this.platform.platforms()),
          'last_login': (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, -1)
        } 
        if(Math.max(...no_peserta)){
          if(this.platform.is('cordova')){
            this.fcm.getToken().then(token => {
              this.current_profile['fcm_token'] = token;
              this.addProfile(this.current_profile);
            });
          }else{
            this.addProfile(this.current_profile);
          }
        }else{
          this.setID(auth2)
        }
      }else{
        //save email user
        this.current_profile['platform'] = JSON.stringify(this.platform.platforms());
        this.current_profile['last_login'] = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, -1);
        this.current_profile['email_google'] = this.current_email;
        this.current_profile['photo_google'] = this.current_image;
        if(!this.user_updated){
          if(this.platform.is('cordova')){
            this.fcm.getToken().then(token => {
              this.current_profile['fcm_token'] = token;
              this.updateProfile(this.current_profile,this.current_profile['id'])
            });
          }else{
            this.updateProfile(this.current_profile,this.current_profile['id'])
          }
          this.user_updated = true;
        }
      }
      loading.dismiss()
      
      if(this.flag){
        if(this.current_profile.kelamin){
          if(this.current_profile.kelamin=='wanita' && !this.current_profile.hasOwnProperty("gmail_wali")){
            this.router.navigate(['/gender']);
          }else{
            this.router.navigate(['/tabs/tab1']);
          }
         } else {
           this.router.navigate(['/gender']);
         }          
      }
    })    
  }

  clear_users(){
    this.all_profile.forEach(user => {
      
      if(user['no_peserta'] != null){
        
      }else {
        this.removeProfile(user.id)
      }
    })
  }

  getProfiles(){
    return this.profiles;
  }

  getProfile(id){
    return this.profilesCollection.doc<profile>(id).valueChanges();
  }

  updateProfile(profile: profile, id: string){
    return this.profilesCollection.doc(id).update(profile)
  }

  addProfile(profile: profile){
    return this.profilesCollection.add(profile)
  }

  setProfile(profile: profile, id: string){
    return this.profilesCollection.doc(id).set(profile)
  }

  removeProfile(id){
    return this.profilesCollection.doc(id).delete()
  }
}
