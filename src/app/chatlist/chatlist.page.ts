import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { chat, ChatService } from '../chat.service';
import { ChatPage } from '../chat/chat.page';


@Component({
  selector: 'app-chatlist',
  templateUrl: './chatlist.page.html',
  styleUrls: ['./chatlist.page.scss'],
})
export class ChatlistPage implements OnInit {

  constructor(public ChatService: ChatService, private modalController: ModalController) { 
    
  }

  ngOnInit() {
  }

  gochat(id){
    this.ChatService.setID(id)
    this.presentModal()
  }

  ngOnDestroy(){
    this.ChatService.inactivePage1 = false;
  }

  kembali(){
    this.ChatService.inactivePage1 = false;
    this.modalController.dismiss();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ChatPage,
      componentProps: { }
    });
    this.ChatService.inactivePage2 = true;
    this.ChatService.inactivePageCount = 2;
    return await modal.present();
  }


}
