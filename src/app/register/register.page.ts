import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  name;
  email;
  password;
  ulangi_password;
  gender;

  constructor(private router: Router, public afAuth: AngularFireAuth) { 
    
  }

  ngOnInit() {
  }

  register(){
    return this.afAuth.auth.createUserWithEmailAndPassword(this.email, this.password);
  }

  login(){
    this.router.navigate(['/login']);    
  }

}
