import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from "angularfire2/firestore";
import { Observable } from 'rxjs';
import { map, max } from 'rxjs/operators'

export interface setting {
  ads_counter: number;
  link_readme: String;
  wa_admin: String;
  app_wallpaper: String;
}

@Injectable({
  providedIn: 'root'
})
export class SettingService {
  private settingsCollection: AngularFirestoreCollection<setting>
  public settings: Observable<setting[]>
  public settings_subscription;
  public all_settings;

  constructor(db: AngularFirestore) {    
    this.settingsCollection =  db.collection<setting>('setting');
    
    this.settings = this.settingsCollection.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data}
        })
      })
    )

    this.settings_subscription = this.getSettings().subscribe(data => {
      this.all_settings = data[0]
    })
  }

  getSettings(){
    return this.settings;
  }

  getSetting(id){
    return this.settingsCollection.doc<setting>(id).valueChanges();
  }

  updateSetting(set: setting, id: string){
    return this.settingsCollection.doc(id).update(set)
  }

  addSetting(set: setting){
    return this.settingsCollection.add(set)
  }

  setSetting(set: setting, id: string){
    return this.settingsCollection.doc(id).set(set)
  }

  removeSetting(id){
    return this.settingsCollection.doc(id).delete()
  }
}
