import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from "angularfire2/firestore";
import { Observable } from 'rxjs';
import { map, max } from 'rxjs/operators'

export interface ad {
  image: String;
  link: String;
  owner_email: String;
  owner_no_peserta: String;
  viewer_age: String;
  viewer_domisili: String;
  viewer_email: String;
  viewer_kelamin: String;
  viewer_no_peserta: String;
  viewer_click_link: String;
}

@Injectable({
  providedIn: 'root'
})
export class AdsService {  
  private adsCollection: AngularFirestoreCollection<ad>
  public ads: Observable<ad[]>
  public ads_subscription;
  public all_ads;

  constructor(db: AngularFirestore) {     
    this.adsCollection =  db.collection<ad>('ads');
    
    this.ads = this.adsCollection.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data}
        })
      })
    )
  }

  getAds(){
    return this.ads;
  }

  getAd(id){
    return this.adsCollection.doc<ad>(id).valueChanges();
  }

  updateAd(ad: ad, id: string){
    return this.adsCollection.doc(id).update(ad)
  }

  addAd(ad: ad){
    return this.adsCollection.add(ad)
  }

  setAd(ad: ad, id: string){
    return this.adsCollection.doc(id).set(ad)
  }

  removeAd(id){
    return this.adsCollection.doc(id).delete()
  }
}
