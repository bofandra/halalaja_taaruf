import { Component, OnInit } from '@angular/core';
import { profile, ProfileService } from '../profile.service';
import { Router } from '@angular/router';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-gender',
  templateUrl: './gender.page.html',
  styleUrls: ['./gender.page.scss'],
})
export class GenderPage implements OnInit {

  public syarat=false;

  constructor(public ProfileService: ProfileService, private router: Router, public alertController: AlertController, 
    public modalCtrl: ModalController, public loadingController: LoadingController) { 
  }

  ngOnInit(  ) {
    
    this.ProfileService.clear_users();
  }

  async presentAlertPrompt() {
    const alert = await this.alertController.create({
      header: 'Masukan alamat Gmail wali/ mahram Anda!',
      inputs: [
        {
          type: 'text',
          id: 'gmail_wali',
          placeholder: '***@gmail.com'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: data => {            
            console.log(data[0]);
            if(data[0].indexOf("@")>-1){
              let email = data[0].replace(/\s/g,'');
              email = data[0].split("@")
              if(email[0].length>2 && email[1]=="gmail.com"){
                this.ProfileService.current_profile['kelamin']="wanita";
                this.ProfileService.current_profile['gmail_wali']=data[0].replace(/\s/g,'');
                this.ProfileService.setProfile(this.ProfileService.current_profile, this.ProfileService.current_profile.id).then(res => {
                  
                  this.router.navigate(['/tabs/tab1']);
                })
              }else{
                this.presentAlert("Format email tidak sesuai!","Masukkan sesuai format ***@gmail.com")
              }
            }else{
              this.presentAlert("Format email tidak sesuai!","Masukkan sesuai format ***@gmail.com")
            }
          }
        }
      ]
    });

    await alert.present();
  }

  async presentAlert(header,message) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }



  async update(gender){
    const loading = await this.loadingController.create({
      message: 'Menyimpan data...',
      translucent: true
    }); 
    
    console.log(gender)

    if(this.syarat==true){
      if(gender=="wanita"){
        this.presentAlertPrompt();
      }else{
        loading.present()    
        this.ProfileService.current_profile['kelamin']=gender;
        this.ProfileService.setProfile(this.ProfileService.current_profile, this.ProfileService.current_profile.id).then(res => {
          loading.dismiss()
          alert("Data jenis kelamin berhasil disimpan")
          this.router.navigate(['/tabs/tab1']);
        })
      }
    }else{
      alert("Anda harus menyetujui perjanjian penggunaan aplikasi!")
    }
  }

}
