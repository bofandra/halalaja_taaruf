import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { profile, ProfileService } from '../profile.service';
import { chat, ChatService } from '../chat.service';
import { setting, SettingService } from '../setting.service';
import { ChatPage } from '../chat/chat.page';
import { ChatlistPage } from '../chatlist/chatlist.page';
import { Platform, ModalController, AlertController, LoadingController } from '@ionic/angular';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { ProposePage } from '../propose/propose.page';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  setting_subscription;
  user;
  profile: profile;
  
  constructor(public loadingController: LoadingController, private localNotifications: LocalNotifications, private router: Router, private route: ActivatedRoute, public ProfileService: ProfileService, 
    public ChatService: ChatService, public SettingService: SettingService,private modalController: ModalController, public alertController: AlertController, private platform: Platform) {
    
  }

  ngOnInit(){
    this.ProfileService.flag = false;
    
    this.ProfileService.clear_users();

    this.ChatService.chats.subscribe(chats=>{
      console.log("chats: "+chats.length);
      this.ChatService.all_chat = chats;
      let i=0;
      this.ChatService.chat_flag = false;
      while(i<chats.length && this.ChatService.chat_flag==false){
        if((chats[i].status=="aktif" || chats[i].status=="diajukan") && chats[i][this.ProfileService.current_profile["kelamin"]]==this.ProfileService.current_profile["no_peserta"]){
            this.ChatService.chat_flag = true;
            this.ChatService.current_chat_id = chats[i]["id"]
            this.ChatService.setID(chats[i]["id"])

            let no_pasangan;
            if(this.ProfileService.current_profile["kelamin"]=="pria"){
              no_pasangan=chats[i]['wanita']
            }else{
              no_pasangan=chats[i]['pria']
            }
            this.localNotifications.schedule({
              id: 1,
              title: "Cek Ta'aruf dengan No."+no_pasangan+" !",
              text: "Klik tombol Ta'aruf di halaman 'Home'",
            }); 
        }
        i++;
      }
    })
  }

  edit_profile(){
    this.router.navigate(['/tabs/tab3']);
  }

  taaruf(){
    this.router.navigate(['/tabs/tab2']);
  }

  admin(){
    this.presentModal2();
  }

  ask(){
    this.presentModal4();
  }
  
  chat(){ 
    if(this.ChatService.current_chat["status"]=="diajukan"){
      if(this.ChatService.current_chat["proposing"]==this.ProfileService.current_profile["no_peserta"]){
        this.presentAlertWaitApproval();
      }else{
        this.presentAlertApproval();
      }
    }else{
      this.presentModal();
    }
  }

  whatsapp(){
    let link_wa = "https://api.whatsapp.com/send?phone=6281290231280&text=Assalamualaikum.%0A%0ASaya%20*No.%20Peserta%20"+this.ProfileService.current_profile["no_peserta"]+"*%2C%20ada%20pertanyaan%20seputar%20aplikasi%20taaruf.halalaja.com%20%3A%0A%0A...";
    let set = this.SettingService.all_settings
    if(set.hasOwnProperty('wa_admin')){
      link_wa = "https://api.whatsapp.com/send?phone="+set["wa_admin"]+"&text=Assalamualaikum.%0A%0ASaya%20*No.%20Peserta%20"+this.ProfileService.current_profile["no_peserta"]+"*%2C%20ada%20pertanyaan%20seputar%20aplikasi%20taaruf.halalaja.com%20%3A%0A%0A..."
      if(this.platform.is('cordova')){
        link_wa = "https://api.whatsapp.com/send?phone="+set["wa_admin"]+"&text=Assalamualaikum.%0A%0ASaya%20*No.%20Peserta%20"+this.ProfileService.current_profile["no_peserta"]+"*%2C%20ada%20pertanyaan%20seputar%20aplikasi%20Ta'aruf%20HalalAja%20%3A%0A%0A..."
      }      
    }
    if(this.platform.is('cordova')){
      //window.open(link_wa, '_blank')
      window.location.assign(link_wa)
    }else{
      window.location.assign(link_wa)
    }
  }

  tutorial(){    
    let link_readme = "http://blog.halalaja.com/";
    let set = this.SettingService.all_settings
    if(set.hasOwnProperty('link_readme')){
      link_readme = ""+set['link_readme']
    }
    if(this.platform.is('cordova')){
      //window.open(link_readme, '_blank')
      window.location.assign(link_readme)
    }else{
      window.location.assign(link_readme)
    }
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ChatPage,
      componentProps: { }
    });
    this.ChatService.setID(this.ChatService.current_chat_id)
    this.ChatService.inactivePage1 = true;
    this.ChatService.inactivePageCount = 1;
    return await modal.present();
  }

  async presentModal4() {
    const modal = await this.modalController.create({
      component: ChatPage,
      componentProps: { }
    });
    this.ChatService.setID("ask")
    this.ChatService.inactivePage1 = true;
    this.ChatService.inactivePageCount = 1;
    return await modal.present();
  }

  async presentModal2() {
    const modal = await this.modalController.create({
      component: ChatlistPage,
      componentProps: { }
    });
    this.ChatService.inactivePage1 = true;
    this.ChatService.inactivePageCount = 1;
    return await modal.present();
  }

  async presentModal3(users, flag_taaruf) {
    const modal = await this.modalController.create({
      component: ProposePage,
      componentProps: { users: users , flag_taaruf: flag_taaruf }
    });
    return await modal.present();
  }

  async presentAlertApproval() {
    const alert = await this.alertController.create({
      header: 'Selamat!',
      message: "Peserta No."+this.ChatService.current_chat["proposing"]+" mengajukan permohonan ta'aruf",
      buttons: [
        {
          text: 'Tolak',
          cssClass: 'secondary',
          handler: data => {
            this.selesai()
          }
        }, {
          text: 'Lihat Profil',
          handler: data => {    
            this.profil(this.ChatService.current_chat["proposing"])
          }
        }, {
          text: 'Terima',
          handler: data => {    
            this.terima();
          }
        }
      ]
    });
    await alert.present();
  }

  async profil(no){
    let i=0;
    let flag=true;
    let users = [];;
       
    const loading = await this.loadingController.create({
      message: 'Membuka profil...',
      translucent: true
    });
    loading.present()

    while(i<this.ProfileService.all_profile.length && flag){
      if(this.ProfileService.all_profile[i].no_peserta==no){
        flag = false;
        users.push(this.ProfileService.all_profile[i])
        loading.dismiss();
        this.presentModal3(users, true)
      }
      i++;
    }
  }
 
  async presentAlertWaitApproval() {
    const alert = await this.alertController.create({
      header: 'Afwan..',
      message: "Peserta No."+this.ChatService.current_chat["proposed"]+" belum menerima permohonan ta'aruf Anda",
      buttons: [
        {
          text: 'Batalkan',
          cssClass: 'secondary',
          handler: data => {
            this.selesai()
          }
        }, {
          text: 'Lihat Profil',
          handler: data => {    
            this.profil(this.ChatService.current_chat["proposed"])
          }
        }, {
          text: 'Tetap Menunggu',
          handler: data => {    
            
          }
        }
      ]
    });
    await alert.present();
  }

  async terima(){
    const loading = await this.loadingController.create({
      message: "Menerima ta'aruf...",
      translucent: true
    });
    loading.present()
    
    let chat = this.ChatService.current_chat;
    chat.status = "aktif";   
    this.ChatService.updateChat(chat, chat["id"]).then(()=>{
      loading.dismiss();
      this.presentModal();
    })
  }

  async selesai(){
    const loading = await this.loadingController.create({
      message: "Membatalkan ta'aruf...",
      translucent: true
    });
    loading.present()
    
    let chat = this.ChatService.current_chat;
    chat.status = "selesai";   
    this.ChatService.updateChat(chat, chat["id"]).then(()=>{
      let flag_pria = true;
      let flag_wanita = true;
      let user_pria, user_wanita;
      let i=0;
      while(i<this.ProfileService.all_profile.length && (flag_pria || flag_wanita) ){
        if(this.ProfileService.all_profile[i].no_peserta==chat['pria']){
          flag_pria = false;
          user_pria = this.ProfileService.all_profile[i];
        }
        if(this.ProfileService.all_profile[i].no_peserta==chat['wanita']){
          flag_wanita = false;
          user_wanita = this.ProfileService.all_profile[i];
        }
        i++;
      }

      user_pria["no_pasangan"]="undefined"   
      user_wanita["no_pasangan"]="undefined"     
      
      this.ProfileService.setProfile(user_pria, user_pria.id).then(res => {
        this.ProfileService.setProfile(user_wanita, user_wanita.id).then(res => {
          loading.dismiss();
        })
      })
    })
  }


}
