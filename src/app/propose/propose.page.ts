import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { profile, ProfileService } from '../profile.service';
import { ModalController, NavParams, AlertController, LoadingController } from '@ionic/angular';
import { chat, ChatService } from '../chat.service';
import { setting, SettingService } from '../setting.service';
import { ChatPage } from '../chat/chat.page';
import { AdsPage } from '../ads/ads.page'; 

import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-propose',
  templateUrl: 'propose.page.html',
  styleUrls: ['propose.page.scss']
})
export class ProposePage {
  id;
  user;
  mode='bio';
  users;
  current_user;
  max_user;
  flag_taaruf;
  setting_subscription;

  constructor(public http: HttpClient, private router: Router, private route: ActivatedRoute, public navParams: NavParams,public alertController: AlertController, private emailComposer: EmailComposer,
    public loadingController: LoadingController, public ProfileService: ProfileService, public SettingService: SettingService, public ChatService: ChatService,private modalController: ModalController){
  }
  
  ngOnInit(){
    this.current_user = 0;
    this.id = this.ProfileService.id;
    this.user = this.ProfileService.current_profile;
    this.users = this.navParams.get('users');
    this.flag_taaruf = false;
    if(this.navParams.get('flag_taaruf')){
      this.flag_taaruf = this.navParams.get('flag_taaruf');
    }
    this.max_user = this.users.length;
  }

  selanjutnya(){
    let ads_counter = 5;
    let set = this.SettingService.all_settings

    if(set.hasOwnProperty('ads_counter')){
      ads_counter = set['ads_counter']
    }
    
    if(this.current_user+1 < this.max_user){
      this.current_user++;
      if(this.current_user % ads_counter == 0){
        this.presentModalAds();
      }
    } else {
      this.current_user = 0;
    }
  }

  kembali(){
    this.modalController.dismiss(); 
  }

  segmentChanged($ev: any){
    //console.log(this.mode)
  }

  async chat(){
    let id;
    let chat;
    if(this.user["kelamin"]=="pria"){
      chat = {
        pria: this.user["no_peserta"],
        wanita: this.users[this.current_user]["no_peserta"],
        photo_pria: this.user["photo_google"],
        photo_wanita: this.users[this.current_user]["photo_google"],
        proposing: this.user["no_peserta"],
        proposed: this.users[this.current_user]["no_peserta"],
        status: "diajukan",
        mulai: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, -1),
        last_chat: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, -1),
        pesan:[{
          waktu: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, -1),
          user: "admin-1",
          isi: "Assalamualaikum.. Akhi & ukhti, silahkan lakukan tanya-jawab sebatas pada hal-hal yg diperlukan & penting saja. Bagi ukhti, pastikan bahwa wali/mahram nya telah melakukan login ke aplikasi menggunakan email yg telah didaftarkan. Selanjutnya, Wali/mahram nya tsb dpt menekan tombol taaruf untuk memantau proses taaruf ini. Jazakumullahu khoiron katsiron..",          
        }]        
      }
      id = this.user["no_peserta"]+"|"+this.users[this.current_user]["no_peserta"];
    }else{
      chat = {
        pria: this.users[this.current_user]["no_peserta"],
        wanita: this.user["no_peserta"],
        photo_pria: this.users[this.current_user]["photo_google"],
        photo_wanita: this.user["photo_google"],
        proposing: this.user["no_peserta"],
        proposed: this.users[this.current_user]["no_peserta"],
        status: "diajukan",
        mulai: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, -1),
        last_chat: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, -1),
        pesan:[{
          waktu: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, -1),
          user: "admin-1",
          isi: "Assalamualaikum.. Akhi & ukhti, silahkan lakukan tanya-jawab sebatas pada hal-hal yg diperlukan & penting saja. Bagi ukhti, pastikan bahwa wali/mahram nya telah melakukan login ke aplikasi menggunakan email yg telah didaftarkan. Selanjutnya, Wali/mahram nya tsb dpt menekan tombol taaruf untuk memantau proses taaruf ini. Jazakumullahu khoiron katsiron..",          
        }]        
      }
      id = this.users[this.current_user]["no_peserta"]+"|"+this.user["no_peserta"];
    } 
    
    if(!this.ChatService.chat_flag){
      //kalau chat baru, create dulu
      if(this.ChatService.setID(id)){        
        const loading = await this.loadingController.create({
          message: "Memulai ta'aruf...",
          translucent: true
        });
        loading.present()
        this.ChatService.setChat(chat,id).then(()=>{
          this.ProfileService.current_profile["no_pasangan"]=this.users[this.current_user]["no_peserta"];
          if(this.users[this.current_user].hasOwnProperty('fcm_token')){
            this.ProfileService.current_profile["fcm_token_pasangan"]=this.users[this.current_user]["fcm_token"];
            this.sendPostRequest(this.users[this.current_user]["fcm_token"]);
          }
          this.sendEmail(this.users[this.current_user]);

          this.ProfileService.setProfile(this.ProfileService.current_profile, this.ProfileService.current_profile.id).then(res => {  
            let user_pasangan;
            user_pasangan = this.users[this.current_user];
            user_pasangan["no_pasangan"]=this.ProfileService.current_profile["no_peserta"];
            if(this.ProfileService.current_profile.hasOwnProperty('fcm_token')){              
              user_pasangan["fcm_token_pasangan"]=this.ProfileService.current_profile["fcm_token"];
            }
            this.ProfileService.setProfile(user_pasangan, user_pasangan.id).then(()=>{
              this.ChatService.setID(id)
              loading.dismiss()
              //this.presentModal();
              
              alert("Permohonan ta'aruf Anda sudah dikirimkan. Cek status ta'aruf di halaman 'Home'.")
              this.modalController.dismiss();
              this.router.navigate(['/tabs/tab1']);
            })                     
          })
        })
      }else{
        this.presentModal();
      }
    }else{
      if(this.ProfileService.current_profile["kelamin"]=="pria"){
        this.presentAlertPrompt(this.ChatService.current_chat["wanita"])
      }else{
        this.presentAlertPrompt(this.ChatService.current_chat["pria"])
      }
    }
    
    //this.router.navigate(['/chat', {id: this.users[this.current_user]["id"]}]);
  }

  sendEmail(user){  
    console.log(JSON.stringify(user))
    if(user['kelamin']=='wanita'){
      
      let email = {
        to: user['email_google'],
        cc: [user['gmail_wali'], 'admin@halalaja.com'],
        bcc: [],
        attachments: [
        ],
        subject: "Permohonan Ta'aruf dari No. Peserta "+this.ProfileService.current_profile['no_peserta'],
        body: "Bersama ini kami informasikan bahwa Anda telah mendapatkan permohonan Ta'aruf dari No. Peserta "+this.ProfileService.current_profile['no_peserta']+". Silahkan buka aplikasi Ta'aruf - HalalAja, lalu klik pada tombol Ta'aruf di halaman depan (home) untuk melanjutkan prosesnya.",
        isHtml: true
      }
      
      // Send a text message using default options
      this.emailComposer.open(email);
    } else {
      let email = {
        to: user['email_google'],
        cc: 'admin@halalaja.com',
        bcc: [],
        attachments: [
        ],
        subject: "Permohonan Ta'aruf dari No. Peserta "+this.ProfileService.current_profile['no_peserta'],
        body: "Bersama ini kami informasikan bahwa Anda telah mendapatkan permohonan Ta'aruf dari No. Peserta "+this.ProfileService.current_profile['no_peserta']+". Silahkan buka aplikasi Ta'aruf - HalalAja, lalu klik pada tombol Ta'aruf di halaman depan (home) untuk melanjutkan prosesnya.",
        isHtml: true
      }
      
      // Send a text message using default options
      this.emailComposer.open(email);
    } 
  }

  sendPostRequest(token) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'key=AAAAduJuFWI:APA91bG4Lh48saf_z9Yf3Z6Zny_-y1ckSkbUwJDPqqFwOxdpEE3WpQQVQN3At-Q66LDQA6_9PJ1lp5tKtaG0pXJCo85mNEY40oL0NuLB1qMDlWvqD6NAbmB2aTflLLXkxeiVX0b9y_pz'
      })
    };

    let notification = {      
      "title":"Cek Ta'aruf dengan No."+this.ProfileService.current_profile["no_peserta"]+" !",
      "click_action":"FCM_PLUGIN_ACTIVITY",
      "body":"Klik tombol Ta'aruf di halaman 'Home'"
    }
    let postData = {
      "notification": notification,
      "to": token
    }
    
    this.http.post("https://fcm.googleapis.com/fcm/send", postData, httpOptions)
      .subscribe(data => {
        //console.log(JSON.stringify(data));
       }, error => {
        //console.log(JSON.stringify(error));
      });
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ChatPage,
      componentProps: { }
    });
    return await modal.present();
  }

  async presentModalAds() {
    const modal = await this.modalController.create({
      component: AdsPage,
      componentProps: { }
    });
    return await modal.present();
  }

  async presentAlertPrompt(no_pasangan) {
    const alert = await this.alertController.create({
      header: "Anda sedang melakukan ta'aruf dengan No. Peserta: "+no_pasangan,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'OK',
          handler: () => {        
            this.presentModal();
          }
        }
      ]
    });
    await alert.present();
  }

  async presentAlertPrompt2(no_pasangan) {
    const alert = await this.alertController.create({
      header: "Anda akan mengajukan ta'aruf dengan peserta No. "+no_pasangan,
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ya, Bismillah..',
          handler: () => {        
            this.chat();
          }
        }
      ]
    });
    await alert.present();
  }
}
