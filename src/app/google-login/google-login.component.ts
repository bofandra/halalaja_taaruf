import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';

import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Platform, AlertController, NavController  } from '@ionic/angular';
import { Router } from '@angular/router'

import { profile, ProfileService } from '../profile.service';


@Component({
  selector: 'app-google-login',
  templateUrl: './google-login.component.html',
  styleUrls: ['./google-login.component.scss']
})
export class GoogleLoginComponent implements OnInit {

  user: Observable<firebase.User>;
  user_subscription;

  constructor(private afAuth: AngularFireAuth, public alertController: AlertController,
    private gplus: GooglePlus, private navCtrl: NavController,
    private platform: Platform,
    private router: Router, public ProfileService: ProfileService) { 
      this.user = this.afAuth.authState;
      this.user_subscription = this.user.subscribe(res => {
        //alert("51... "+JSON.stringify(res))
        if(res != null){
          if (this.platform.is('cordova')) {
            if(res["uid"]){
              if(this.ProfileService.flag){
                this.ProfileService.setID(res);   
              }
              //this.router.navigate(['/gender']);
            }
          } else {
            if(res["uid"]){
              if(this.ProfileService.flag){
                this.ProfileService.setID(res);   
              }
              //this.router.navigate(['/gender']);
            }
          }     
        }    
      })
  }  

  async presentAlertApproval() {
    const alert = await this.alertController.create({
      message: 'Apakah Anda ingin keluar dari aplikasi?',
      buttons: [
        {
          text: 'Tidak',
          role: 'cancel',
          cssClass: 'secondary',
          handler: data => {
            console.log("cancel")
          }
        }, {
          text: 'Ya',
          handler: data => {    
            this.signOut();
          }
        }
      ]
    });
    await alert.present();
  }

  googleLogin() {
    if (this.platform.is('cordova')) {
      this.nativeGoogleLogin();
      //this.webGoogleLogin2();
    } else {
      this.webGoogleLogin();
    }
  }

  async nativeGoogleLogin() {
    try {

      const gplusUser = await this.gplus.login({
        'webClientId': '510605006178-5lh1bib3le5d7g15hciqph9v4mhms8f9.apps.googleusercontent.com',
        'offline': true,
        'scopes': 'profile email'
      })

      return await this.afAuth.auth.signInWithCredential(
        firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken)
      )


    } catch(err) {
      alert(JSON.stringify(err))
    }
  }

  async webGoogleLogin(): Promise<void> {
    try {
      const provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');
      const credential = await this.afAuth.auth.signInWithPopup(provider);
    } catch(err) {
      console.log(err)
    }
  
  }

  ngOnInit() {
  }

  signOut() {
    this.afAuth.auth.signOut();
    this.user_subscription.unsubscribe();
    this.ProfileService.profile_subscription.unsubscribe();
    this.ProfileService.flag = true;
    this.router.navigate(['/']);
  }

}
