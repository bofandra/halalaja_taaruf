import { Component, OnInit } from '@angular/core';
import { profile, ProfileService } from '../profile.service';
import { Router, ActivatedRoute } from '@angular/router'
import { LoadingController, ModalController } from '@ionic/angular';

import { EditprofilePage } from '../editprofile/editprofile.page';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  id;
  user;

  constructor(public ProfileService: ProfileService, private router: Router, private route: ActivatedRoute,
    public loadingController: LoadingController, public modalController: ModalController){
      
  }  

  ngOnInit(){
    this.ProfileService.flag = false;
    this.id = this.ProfileService.id;
    this.user = this.ProfileService.current_profile;
  }

  edit_bio(){
    //this.router.navigate(['/editprofile', { user: this.user}]);
    this.presentModal('bio');
  }

  edit_cv(){
    this.presentModal('cv');
  }

  photo_google(){
    window.location.assign("https://support.google.com/mail/answer/35529?co=GENIE.Platform%3DAndroid&hl=en")
  }

  edit_personal(){
    this.presentModal('personal');
  }

  async presentModal(mode) {
    const modal = await this.modalController.create({
      component: EditprofilePage,
      componentProps: { mode: mode }
    });
    return await modal.present();
  }
}
