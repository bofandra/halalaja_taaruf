import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { ProposePage } from '../propose/propose.page';
import { ModalController, AlertController, LoadingController } from '@ionic/angular';
import { profile, ProfileService } from '../profile.service';
import { chat, ChatService } from '../chat.service';

@Component({
  selector: 'app-tab2',
  templateUrl: './tab2.page.html',
  styleUrls: ['./tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  user;
  status="active";

  constructor(public loadingController: LoadingController, public ChatService: ChatService, public alertController: AlertController, public ProfileService: ProfileService, private router: Router, private route: ActivatedRoute, public modalController: ModalController) { }

  ngOnInit() {
    this.user = {
      kulit:"undefined",
      suku: "",
      domisili: "",
      status_kawin: "undefined",
      pendidikan: "undefined",
      pekerjaan: "undefined"
    }

    if(this.ChatService.chat_flag == false){
      this.selesai();
    }
  }

  async selesai(){
    const loading = await this.loadingController.create({
      message: 'Mempersiapkan data...',
      translucent: true
    });
    loading.present()

    this.ProfileService.current_profile["no_pasangan"]="undefined"       
    this.ProfileService.setProfile(this.ProfileService.current_profile, this.ProfileService.current_profile.id).then(res => {
        loading.dismiss();
    })
  }

  cek_profil(){ 
    this.ProfileService.cek_profile();
    if(this.ProfileService.unfilled.length>0){
      this.presentAlertPrompt(this.ProfileService.unfilled.join(", "))
    }else{
      if(this.ProfileService.current_profile.hasOwnProperty("no_pasangan")){
        if(this.ProfileService.current_profile["no_pasangan"]!="undefined" && this.ProfileService.current_profile["no_pasangan"]!= null ){
          alert("Anda sedang melakukan proses ta'aruf dengan peserta No."+this.ProfileService.current_profile["no_pasangan"]+". Klik tombol ta'aruf di halaman 'Home'")
          this.router.navigate(['/tabs/tab1']);
        }else{
          this.propose();
        }
      }else{
        this.propose();
      }
    }
  }

  propose(){
    let users = [];
    let pasangan = "pria";
    
    if(this.ProfileService.current_profile.kelamin=="pria"){
      pasangan = "wanita";
    }else{
      pasangan = "pria";
    }
    
    this.ProfileService.all_profile.forEach(user => {
      let user_flag = true;
      if(user.kelamin != pasangan){
        user_flag = false;
      }
	  if(user.no_peserta == null){
        user_flag = false;
		this.ProfileService.removeProfile(user.id)
      }
      if(user.kulit != this.user.kulit && this.user.kulit!='undefined'){
        user_flag = false;
      }
      if(user.pendidikan != this.user.pendidikan && this.user.pendidikan!='undefined'){
        user_flag = false;
      } 
      if(user.pekerjaan != this.user.pekerjaan && this.user.pekerjaan!='undefined'){
        user_flag = false;
      }
      if(user.status_kawin != this.user.status_kawin && this.user.status_kawin!='undefined'){
        user_flag = false;
      }
      if(user.hasOwnProperty("no_pasangan")){
        if(user.no_pasangan!="undefined" && user.no_pasangan!=null){
          user_flag = false;
        }
      }
      if(!user.hasOwnProperty("last_login")){
        user_flag = false;
      }
      if(this.user.suku!=""){
        if(user.hasOwnProperty("suku")){
          if(!user.suku.toLowerCase().match(this.user.suku.toLowerCase())){
            user_flag = false;
          }
        }else{
          user_flag = false;
        }
      }
      if(this.user.domisili!=""){
        if(user.hasOwnProperty("domisili")){
          if(!user.domisili.toLowerCase().match(this.user.domisili.toLowerCase())){
            user_flag = false;
          }
        }else{
          user_flag = false;
        }
      }
      if(user_flag){
        if(user.hasOwnProperty("kulit") && user.hasOwnProperty("pendidikan") && 
          user.hasOwnProperty("pekerjaan") && user.hasOwnProperty("status_kawin")){
          users.push(user);
        }
      }
    });
    if(users.length>0){
      alert(users.length+" Calon ditemukan")
      users  = users.sort(function(a, b) {
        a = new Date(a.last_login);
        b = new Date(b.last_login);
        return a>b ? -1 : a<b ? 1 : 0;
      });
      this.presentModal(users);
    }else{
      alert("Calon tidak ditemukan")
    }
  }

  async presentAlertPrompt(fields) {
    const alert = await this.alertController.create({
      header: "Anda belum melengkapi data profil",
      message: fields,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {        
            this.router.navigate(['/tabs/tab3']);
          }
        }
      ]
    });
    await alert.present();
  }

  async presentModal(users) {
    const modal = await this.modalController.create({
      component: ProposePage,
      componentProps: { users: users }
    });
    return await modal.present();
  }

}
