import { Component, OnInit } from '@angular/core';
import { profile, ProfileService } from '../profile.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(public ProfileService: ProfileService) { 
  }

  ngOnInit() {
    this.ProfileService.flag = true;
  }

}
