import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ad, AdsService } from '../ads.service';
import { profile, ProfileService } from '../profile.service';

@Component({
  selector: 'app-ads',
  templateUrl: './ads.page.html',
  styleUrls: ['./ads.page.scss'],
})
export class AdsPage implements OnInit {
  public ads_subscription;
  public all_ads;
  public current_ad;

  constructor(private modalController: ModalController, public AdsService: AdsService, public ProfileService: ProfileService) { }

  ngOnInit() {
    
    
    this.ads_subscription = this.AdsService.ads.subscribe(ads => {
      this.all_ads = ads;
      this.current_ad = this.getRandomInt(0, this.all_ads.length-1)
      document.getElementById("belakang").style.backgroundImage = "url('"+this.all_ads[this.current_ad].image+"')"
    })
  } 

  tutup(){
    this.ads_subscription.unsubscribe();
    this.modalController.dismiss();

    if(this.ProfileService.current_profile){
      this.all_ads[this.current_ad]['viewer_email'] = this.all_ads[this.current_ad]['viewer_email'] + " , "+ this.ProfileService.current_profile['email_google'];
      this.all_ads[this.current_ad]['viewer_domisili'] = this.all_ads[this.current_ad]['viewer_domisili'] + " , "+ this.ProfileService.current_profile['domisili'];
      this.all_ads[this.current_ad]['viewer_kelamin'] = this.all_ads[this.current_ad]['viewer_kelamin'] + " , "+ this.ProfileService.current_profile['kelamin'];
      this.all_ads[this.current_ad]['viewer_age'] = this.all_ads[this.current_ad]['viewer_age'] + " , "+ this.ProfileService.current_profile['dob'];
      this.all_ads[this.current_ad]['viewer_click_link'] = this.all_ads[this.current_ad]['viewer_click_link'] + " , "+ "no";
  
      this.AdsService.updateAd(this.all_ads[this.current_ad],this.all_ads[this.current_ad]['id'])
    }
  }

  link(){
    if(this.current_ad){
      window.location.assign(this.all_ads[this.current_ad].link)

      if(this.ProfileService.current_profile){
        this.all_ads[this.current_ad]['viewer_email'] = this.all_ads[this.current_ad]['viewer_email'] + " , "+ this.ProfileService.current_profile['email_google'];
        this.all_ads[this.current_ad]['viewer_domisili'] = this.all_ads[this.current_ad]['viewer_domisili'] + " , "+ this.ProfileService.current_profile['domisili'];
        this.all_ads[this.current_ad]['viewer_kelamin'] = this.all_ads[this.current_ad]['viewer_kelamin'] + " , "+ this.ProfileService.current_profile['kelamin'];
        this.all_ads[this.current_ad]['viewer_age'] = this.all_ads[this.current_ad]['viewer_age'] + " , "+ this.ProfileService.current_profile['dob'];
        this.all_ads[this.current_ad]['viewer_click_link'] = this.all_ads[this.current_ad]['viewer_click_link'] + " , "+ "yes";
  
        this.AdsService.updateAd(this.all_ads[this.current_ad],this.all_ads[this.current_ad]['id'])
      }
    }else{
      window.location.assign("https://bit.ly/2JtYzIA")
    }
  }

  getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

}
