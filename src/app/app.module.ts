import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { FCM } from '@ionic-native/fcm/ngx';

import { ComponentsModule } from './components.module';
import { EditprofilePageModule } from './editprofile/editprofile.module'
import { ProposePageModule } from './propose/propose.module'
import { ChatPageModule } from './chat/chat.module'
import { ChatlistPageModule } from './chatlist/chatlist.module'
import { AdsPageModule } from './ads/ads.module'

import { ProfileService } from './profile.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, EditprofilePageModule, ChatlistPageModule, ProposePageModule, ChatPageModule,AdsPageModule,
    AngularFireModule.initializeApp(environment.firebase), HttpClientModule,HttpModule,
    AngularFirestoreModule, AngularFireAuthModule, CommonModule, ComponentsModule
  ],
  providers: [
    FCM,
    EmailComposer,
    LocalNotifications,
    ProfileService,
    GooglePlus,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
