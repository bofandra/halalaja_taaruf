import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { chat, ChatService } from '../chat.service';
import { profile, ProfileService } from '../profile.service';
import { ModalController, NavParams, LoadingController } from '@ionic/angular';
import { ProposePage } from '../propose/propose.page';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  user;
  id;
  chat;
  teks;
  constructor(public loadingController: LoadingController, public navParams: NavParams, public ChatService: ChatService, public ProfileService: ProfileService,private router: Router, private route: ActivatedRoute, private modalController: ModalController) { }

  ngOnInit() {
    this.user = this.ProfileService.current_profile; 
    this.chat = this.ChatService.current_chat;
  }

  ngOnDestroy(){
    if(this.ChatService.inactivePageCount == 2){
      this.ChatService.inactivePage2 = false;
      this.ChatService.inactivePageCount--;
    }
    if(this.ChatService.inactivePageCount == 1){
      this.ChatService.inactivePage1 = false;
    }
  }

  sendMessage(){
    let pesan = {
      waktu: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, -1),
      user: this.user["no_peserta"],
      isi: this.teks, 
    }
    if(this.ProfileService.current_profile.hasOwnProperty('admin')){
      pesan = {
        waktu: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, -1),
        user: "admin-"+this.user["admin"],
        isi: this.teks, 
      }
    }
    this.chat["pesan"].push(pesan);
    this.chat["last_chat"] = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, -1),
    this.ChatService.updateChat(this.chat, this.chat["id"]).then(()=>{
      this.teks = "";
    })
  }

  kembali(){
    if(this.ChatService.inactivePageCount == 2){
      this.ChatService.inactivePage2 = false;
      this.ChatService.inactivePageCount--;
    }
    if(this.ChatService.inactivePageCount == 1){
      this.ChatService.inactivePage1 = false;
    }
    this.modalController.dismiss();
  }

  async profil(no){
    let i=0;
    let flag=true;
    let users = [];;
       
    const loading = await this.loadingController.create({
      message: 'Membuka profil...',
      translucent: true
    });
    loading.present()

    while(i<this.ProfileService.all_profile.length && flag){
      if(this.ProfileService.all_profile[i].no_peserta==no){
        flag = false;
        users.push(this.ProfileService.all_profile[i])
        loading.dismiss();
        this.presentModal(users, true)
      }
      i++;
    }
  }

  async presentModal(users, flag_taaruf) {
    const modal = await this.modalController.create({
      component: ProposePage,
      componentProps: { users: users , flag_taaruf: flag_taaruf }
    });
    return await modal.present();
  }

  async selesai(){
    this.chat.status = "selesai";     
    const loading = await this.loadingController.create({
      message: 'Mengakhiri percakapan...',
      translucent: true
    });
    loading.present()
    this.ChatService.updateChat(this.chat, this.chat["id"]).then(()=>{
      let flag_pria = true;
      let flag_wanita = true;
      let user_pria, user_wanita;
      let i=0;
      while(i<this.ProfileService.all_profile.length && (flag_pria || flag_wanita) ){
        if(this.ProfileService.all_profile[i].no_peserta==this.chat['pria']){
          flag_pria = false;
          user_pria = this.ProfileService.all_profile[i];
        }
        if(this.ProfileService.all_profile[i].no_peserta==this.chat['wanita']){
          flag_wanita = false;
          user_wanita = this.ProfileService.all_profile[i];
        }
        i++;
      }

      user_pria["no_pasangan"]="undefined"   
      user_wanita["no_pasangan"]="undefined"     
      
      this.ProfileService.setProfile(user_pria, user_pria.id).then(res => {
        this.ProfileService.setProfile(user_wanita, user_wanita.id).then(res => {
          loading.dismiss();
          this.modalController.dismiss();
        })
      })
    })
  }

}
