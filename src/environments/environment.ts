// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDpGWU-GU1W6Nx5DrzTw8yy5mNC9besYdM",
    authDomain: "halalaja-taaruf.firebaseapp.com",
    databaseURL: "https://halalaja-taaruf.firebaseio.com",
    projectId: "halalaja-taaruf",
    storageBucket: "halalaja-taaruf.appspot.com",
    messagingSenderId: "510605006178"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
